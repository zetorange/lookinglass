﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("AsyncUsage", "AsyncFixer02:Long running or blocking operations under an async method", Justification = "<Pending>", Scope = "member", Target = "~M:OrchardCore.DynamicCache.Liquid.CacheStatement.WriteToAsync(System.IO.TextWriter,System.Text.Encodings.Web.TextEncoder,Fluid.TemplateContext)~System.Threading.Tasks.Task{Fluid.Ast.Completion}")]

