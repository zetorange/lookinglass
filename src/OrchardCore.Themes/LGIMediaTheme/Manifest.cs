using OrchardCore.DisplayManagement.Manifest;

[assembly: Theme(
    Name = "LGI Media Theme",
    Author = "Digital Affinity",
    Website = "https://digitalaffinity.com.au",
    Version = "2.0.0",
    Description = "A theme for looking glass international."
)]